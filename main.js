'use strict';

function main(argv) {
  const Parser = require('./src/parser').Parser;
  const Writer = require('./src/writer').Writer;
  const readFileSync = require('fs').readFileSync;
  const writeFileSync = require('fs').writeFileSync;

  let arg = require('minimist')(argv)
    .options({
      strict: {
        alias: 's',
        default: true
      }
    })
    .boolean('strict')
    .usage('Usage: tsd2cce [definition file] [externs file]')
    .demand(2)
    .argv;

  let parser = new Parser(argv._[0], readFileSync(argv._[0]).toString());
  let writer = new Writer(parser.ast, {
    strict: argv.strict
  });

  writeFileSync(argv._[1], writer.toCode());
}

exports.main = main;